//node server for Pooper API
//notes: Server calls DB every nth seconds to expose API's for pooper front end

//require packages
const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const moment = require('moment');
var fs = require('fs');
const https = require('https')


const app = express();


const privateKey = fs.readFileSync('/etc/letsencrypt/live/data-frontiers.info/privkey.pem');
const certificate = fs.readFileSync('/etc/letsencrypt/live/data-frontiers.info/fullchain.pem');

const credentials = {
	key: privateKey,
	cert: certificate
};

//create stamp to update
var stamp;
var stamp1 = moment().utcOffset('-0500').format("YYYY-MM-DD HH:mm:ss").substr(0,18)+'0';
function updateStamp() {
    stamp = moment().utcOffset('-0500').format("YYYY-MM-DD HH:mm:ss").substr(0,18)+'0';
    stampDayBack = moment().utcOffset('-0500').subtract(24, "hours").format("YYYY-MM-DD HH:mm:ss").substr(0,18)+'0';
    stampHourBack = moment().utcOffset('-0500').subtract(1, "hours").format("YYYY-MM-DD HH:mm:ss").substr(0,18)+'0';
    //console.log('now is...', stamp);
    //console.log('a day ago is...', stampDayBack);
    //console.log('an hour ago is...', stampHourBack);
}

//update stamp every 15 seconds
updateStamp();
setInterval(() => {
    updateStamp();
}, 15000);


//connection for database
var connection = mysql.createConnection({
    host : "nikitatest.ctxqlb5xlcju.us-east-2.rds.amazonaws.com",  
    user : "fallenangel1",
    password : "nvoevodin",
    database : "nikita99",

});

//test connection
connection.connect(err =>{
    if(err){
        return err;
    }
});


// call cors
app.use(cors());



app.get('/', (req,res) => {
    res.send('hello from monitoring app')
});


//////////////
let someVar = [];
function setValue(value) {
    someVar = value;
    
  }

  setInterval(() => {
  console.log(stamp)

    connection.query('SELECT sum(poops) as poops, time, lat, lon FROM poops where time = ' + '"'+stamp+'"' + ' group by time, lat, lon', (err, rows) => {
          if (err) {
              throw err;
          } else {
              setValue(rows)
              //console.log(rows)
          }
      }); 


      app.get('/stats', async (req, res) => {
        res.send(someVar)
   
    });
}, 15000);
/////////////////


let sum = [];
function setValue1(value) {
    sum = value; 
  }
    connection.query('SELECT sum(poops) as poops FROM poops where time <= ' + '"'+stamp1+'"' + ' ', (err, rows) => {
        if (err) {
            throw err;
        } else {
            setValue1(rows)
            //console.log(rows)
        }
    });
    app.get('/sum', async (req, res) => {
        res.send(sum)

});


app.get('/poops/:time/:time1',  cors(), function(req,res){
    var time = req.params.time;
    var time1 = req.params.time1;
    console.log(time)
    var sql = "SELECT * FROM poops WHERE time > '"+time+"' and time <= '"+time1+"'";
    connection.query(sql, time, function(err, results) {
        if(err) {
            return res.send(err)
        } else {
            console.log(results)
            return res.json({
                data: results
            })
        }
    });
})

//add a record 
app.post('/add', (req, res) => {
    
    var my_data = {
        time: req.query.time,
        lat: req.query.lat,
        lon: req.query.lon,
        floor: req.query.floor,
        type: 'R',
        poops: req.query.poops,
        beer: req.query.beer,
        min: req.query.min,
        diet: req.query.diet,
        shape: req.query.shape
       }
       
       // now the createStudent is an object you can use in your database insert logic.
       connection.query('INSERT INTO poops SET ?', my_data, function (err, results) {
        if(err) {
            console.log(err)
            return res.send(err)
            
        } else {
            console.log(results)
            return res.json({
                data: results
            })
        }
    });
});




app.get('/ppday/:time/:time1', cors(), (req, res) => {

    var time = req.params.time;
    var time1 = req.params.time1;

    const SELECT_ALL = "SELECT DATE_FORMAT(time, '%Y-%m-%d') as day, count(*) as poops FROM poops where time >= '"+time+"' and time <= '"+time1+"' group by DATE_FORMAT(time, '%Y-%m-%d')";

    connection.query(SELECT_ALL, (err, results) => {
        if (err) {
            console.error(err);

            return res.send(err)
        } else {
            return res.json({
                data: results
            })
        }
    });
});

//poops per hour and day (this will be reduced locally to by day for other graph)
app.get('/pphour/:time/:time1', cors(), (req, res) => {

    var time = req.params.time;
    var time1 = req.params.time1;

    const SELECT_ALL = 
    "SELECT DATE_FORMAT(time, '%Y-%m-%d') as day, HOUR(time) as hour, count(*) as poops FROM poops where time >= '"+time+"' and time <= '"+time1+"' group by DATE_FORMAT(time, '%Y-%m-%d'), HOUR(time)";

    connection.query(SELECT_ALL, (err, results) => {
        if (err) {
            console.error(err);

            return res.send(err)
        } else {
            return res.json({
                data: results
            })
        }
    });
});



//day and hour pulls as variables all couched in functions-----------------------------------------------------
//functions run once first for immediate access, then update every 5 minutes

//delay time
const delay = 300000;

// query daily counts
let dailyCounts = [];
setDailyValue = (value) => {
    dailyCounts = value;
}

fetchDaily = () => {
    console.log(`fetching dailies every ${delay/60000} mins...`)
    connection.query('SELECT DATE_FORMAT(time, "%Y-%m-%d") as day, count(*) as poops  FROM poops where time <= ' + '"'+stamp+'"' + ' group by DATE_FORMAT(time, "%Y-%m-%d") ', (err, rows) => {
        if (err) {
            throw err;
        } else {
            setDailyValue(rows)
        }
    });

    //expose apis
    app.get('/ppday', async (req, res) => {
            res.send(dailyCounts)
    });
}

//query hourly counts
let hourlyCounts = [];
setHourlyValue = (value) => {
    hourlyCounts = value;
}
fetchHourly = () => {
    console.log(`fetching hourlies every ${delay/60000} mins...`)
    connection.query('SELECT DATE_FORMAT(time, "%Y-%m-%d") as day, HOUR(time) as hour, count(*) as poops FROM poops where time >=  ' + '"'+stampDayBack+'"' + ' AND  time <= ' + '"'+stamp+'"' + ' group by DATE_FORMAT(time, "%Y-%m-%d"), HOUR(time) ', (err, rows) => {
        if (err) {
            throw err;
        } else {
            setHourlyValue(rows)
        }
    });

    app.get('/pphour', async (req, res) => {
        res.send(hourlyCounts)
    });
}

// run once first and then set to run every 5 minutes
fetchDaily();
fetchHourly();
setInterval(() => {

    //fetch data
    fetchDaily();
    fetchHourly();

},delay);




// Starting  https server
//const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

// httpServer.listen(80, () => {
// 	console.log('HTTP Server running on port 80');
// });

httpsServer.listen(4500, () => {
	console.log('HTTPS Server running on port 4500');
});







// https.createServer({
    
//     key: fs.readFileSync('/etc/letsencrypt/live/data-frontiers.info/privkey.pem'),
// cert: fs.readFileSync('/etc/letsencrypt/live/data-frontiers.info/fullchain.pem')
    
    
// }, 

// app).listen(4500, () => {
//     console.log('Listening...')
//   })

// app.listen(4500,()=>{
//     console.log(`products server is listening port 4500`)
// });




/**
 * old code
 * 
 * // query(ALL,'/all');
// query(TOT_POOPS,'/live_poops');

// query(FEEDBACK,'/feed');
// query(ALL_COUNT,'/count');
// query(CURRENT_RATING,'/rating');
// query(REPORTS,'/reports');
// query(DOWNLOADS,'/downloads');
// query(TOTTIME,'/tottime');
// query(NUKES,'/nukes');


// app.get('/stats', async (req, res) => {
//     const SELECT_ALL = 'SELECT sum(poops) as poops, time, lat, lon FROM poops where time = ' + '"'+stamp+'"' + ' group by time, lat, lon';

//     connection.query(SELECT_ALL, (err, results) => {
//         if (err) {
//             console.error(err);

//             return res.send(err)
//         } else {
//             return res.json({
//                 data: results
//             })
//         }
//     });
// });

 * 
// var query = (q,a) =>{

//     app.get(a, (req,res)=>{
//         connection.query(q, (err,results)=>{
//             if(err){
//                 return res.send(err)
//             } else {
//                 return res.json({
//                     data: results
//                 })
//             }
//         });
    
//     });

// };


// query(SELECT_ALL,'/stats');



// var SELECT_ALL = 'SELECT sum(poops) as poops, time, lat, lon FROM poops where time = ' + '"'+stamp+'"' + ' group by time, lat, lon';
//const SELECT_ALL = 'SELECT sum(poops) as poops, time, lat, lon FROM poops group by time, lat, lon';
// const SELECT_ALL = 'SELECT sum(count) as count, DATE_FORMAT(time, "%Y-%m-%d %H:00:00") as dateTime FROM monitor_visits group by DATE_FORMAT(time, "%Y-%m-%d %H:00:00")';
// const ALL_COUNT = 'SELECT sum(count) as count FROM monitor_visits';
// const FEEDBACK = 'SELECT count(time) as count FROM monitor_feedback'
// const CURRENT_RATING = 'SELECT avg(convert(substring(grade,1,1), unsigned integer)) as count FROM monitor_feedback'
// const REPORTS = 'select (SELECT sum(count) FROM monitor_report) + (SELECT sum(count) FROM monitor_report1) as count'
// const DOWNLOADS = 'SELECT count(*) as count FROM monitor_download'
// const TOTTIME = 'SELECT round(sum(time)/60) as count FROM visit_time'

// const NUKES = "SELECT country,latitude,longitude,date_long,name,region FROM Nukes"

 */